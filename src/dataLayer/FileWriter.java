package dataLayer;

import businessLayer.MenuItem;
import businessLayer.Order;
import businessLayer.Restaurant;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

public class FileWriter {

    public static void writeBillToFile(Order givenOrder) {
        List<MenuItem> currentOrderProducts = Restaurant.getMappingOrders().get(givenOrder);
        String orderFilename = "Order" + givenOrder.getOrderID() + ".txt";

        try {
            PrintWriter writer = new PrintWriter(orderFilename, "UTF-8");
            writer.println("Ordered products:");

            double finalBillCost = 0;
            for (MenuItem item : currentOrderProducts) {
                writer.println(item.getName() + " : " + item.computePrice() + " RON");
                finalBillCost += item.computePrice();
            }

            writer.println("Total bill cost: " + finalBillCost + " RON");

            writer.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException exc) {
            exc.printStackTrace();
        }
    }
}
