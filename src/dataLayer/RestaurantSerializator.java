package dataLayer;

import java.awt.*;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;

import businessLayer.MenuItem;
import businessLayer.Restaurant;
import controller.MainController;

public class RestaurantSerializator {
    String filename = "restaurant.ser";
    ObjectOutputStream output;
    ObjectInputStream input;

    public void RestaurantSerializationOUT(ArrayList<MenuItem> a) {
        try {
            FileOutputStream givenFile = new FileOutputStream(filename);
            output = new ObjectOutputStream(givenFile);
            output.writeObject(a);

            output.close();
            givenFile.close();
        } catch(Exception exc) {
            System.out.println(exc.getMessage());
        }
    }

    public ArrayList<MenuItem> RestaurantDeserializationIN() {
        //Restaurant restaurant = MainController.getRestaurant();

        try {
            FileInputStream givenFile = new FileInputStream(filename);
            input = new ObjectInputStream(givenFile);
            ArrayList<MenuItem> currentObject = (ArrayList<MenuItem>) input.readObject();

            input.close();
            givenFile.close();

            return currentObject;
        } catch(Exception exc) {
            System.out.println(exc.getMessage());
        }

        return null;
    }
}

