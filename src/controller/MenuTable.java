package controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;

import businessLayer.MenuItem;
import businessLayer.Restaurant;

public class MenuTable {
    private Restaurant r;
    JFrame t = new JFrame();
    JButton ok = new JButton("OK");

    public MenuTable(Restaurant r) {
        this.r = r;
        ArrayList<MenuItem> lista = this.r.getMenu();
        String[] column = {"Name", "Price"};
        int size = lista.size();
        String[][] data = new String[size][20];
        int index = 0;
        if (lista!=null) {
            for(MenuItem m: lista) {
                data[index][0] = m.getName();
                data[index][1] = Double.toString(m.getTotalPrice());
                index++;
            }
        }
        JTable table = new JTable(data,column);
        table.setBounds(60,100,900,500);
        JScrollPane sp = new JScrollPane(table);
        sp.setSize(600,600);
        t.setSize(600, 600);
        JPanel content = new JPanel();
        content.setLayout(new BoxLayout(content, BoxLayout.Y_AXIS));
        content.add(sp);
        content.add(ok);
        ok.addActionListener(new OKActionListener());
        t.setContentPane(content);
        t.setSize(500,400);
        t.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        t.setTitle("Products");
        t.setVisible(true);
    }
    public class OKActionListener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            t.setVisible(false);
        }
    }
}
