package controller;

import businessLayer.*;
import businessLayer.MenuItem;
import controller.MenuTable;
import controller.OrderTable;
import presentationLayer.AdministratorGraphicalUserInterface;
import presentationLayer.ChoiceGUI;
import presentationLayer.WaiterGraphicalUserInterface;
import presentationLayer.ChefGraphicalUserInterface;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class MainController {
    private Restaurant restaurant;
    private ChoiceGUI chooseGUI;
    private AdministratorGraphicalUserInterface adminGUI;
    private WaiterGraphicalUserInterface waiterGUI;
    private ChefGraphicalUserInterface chefGUI;

    public MainController(ChoiceGUI chooseGUI, AdministratorGraphicalUserInterface adminGUI, WaiterGraphicalUserInterface waiterGUI, ChefGraphicalUserInterface chefGUI, Restaurant restaurant) {
        this.chooseGUI = chooseGUI;
        this.adminGUI = adminGUI;
        this.waiterGUI = waiterGUI;
        this.chefGUI = chefGUI;
        this.restaurant = restaurant;

        chooseGUI.getFrame().setVisible(true);

        this.chooseGUI.addActionListenerWaiter(new ActionListenerWaiter());
        this.chooseGUI.addActionListenerAdministrator(new ActionListenerAdministrator());

        this.adminGUI.addInsertActionListener(new ActionListenerInsertAdmin());
        this.adminGUI.addDeleteActionListener(new ActionListenerDeleteAdmin());
        this.adminGUI.addUpdateActionListener(new ActionListenerUpdateAdmin());
        this.adminGUI.addListMenuItemsActionListener(new ActionListenerListMenuItems());
        this.adminGUI.addOKActionListener(new OKActionListenerAdmin());

        this.waiterGUI.addInsertActionListener(new ActionListenerInsertWaiter());
        this.waiterGUI.computePriceActionListener(new ActionListenerComputePrice());
        this.waiterGUI.generateBillActionListener(new ActionListenerGenerateBill());
        this.waiterGUI.addOKActionListener(new ActionListenerOKWaiter());
        this.waiterGUI.viewAllOrdersActionListener(new ActionListenerViewAllOrders());

        this.chefGUI.addOKActionListener(new ActionListenerOKChef());
    }

    public Restaurant getRestaurant() {
        return restaurant;
    }

    public class ActionListenerWaiter implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            chooseGUI.getFrame().setVisible(false);
            waiterGUI.getFrame().setVisible(true);
        }
    }

    public class ActionListenerAdministrator implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            chooseGUI.getFrame().setVisible(false);
            adminGUI.getFrame().setVisible(true);
        }
    }

    public class ActionListenerInsertAdmin implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            // create new menu item
            String givenName = adminGUI.getName();
            String givenName2 = adminGUI.getName2();
            double givenPrice = Double.parseDouble(adminGUI.getPrice());
            businessLayer.MenuItem newMenuItem;

            if(givenName.equals("") && givenName2.equals("")) {//the user did not enter any name before wanting to create a new item
                adminGUI.warningInsert(); //notice the user to enter a valid name before attempting create the item
                return;
            }

            if (givenPrice == 0) {
                newMenuItem = new CompositeProduct(givenName2, givenPrice);
                List<MenuItem> chosenProducts = adminGUI.getChosenComponents().getSelectedValuesList();

                for (MenuItem item : chosenProducts)
                    ((CompositeProduct) newMenuItem).addItem(item);

                newMenuItem.computePrice();
                restaurant.createNewMenuItem(newMenuItem);

                // update fields(for aesthetic purposes)
                adminGUI.getChosenComponents().setListData(restaurant.getMenu().toArray());
                waiterGUI.getOrderedProducts().setListData(restaurant.getMenu().toArray());
                adminGUI.setPrice("0");
                adminGUI.setName2TextField("");
            } else {
                newMenuItem = new BaseProduct(givenName, givenPrice);
                restaurant.createNewMenuItem(newMenuItem);

                // update fields(for aesthetic purposes)
                adminGUI.getChosenComponents().setListData(restaurant.getMenu().toArray());
                waiterGUI.getOrderedProducts().setListData(restaurant.getMenu().toArray());
                adminGUI.setPrice("0");
                adminGUI.setNameTextField("");
            }
        }
    }

    public class ActionListenerDeleteAdmin implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            // delete menu item
            String name = adminGUI.getName();
            String name2 = adminGUI.getName2();
            MenuItem foundItem = null;

            if(!name.equals("") && name2.equals("")) { //a menuItem of type base product is going to be deleted
                Iterator<MenuItem> iterator = restaurant.getMenu().iterator();
                while (iterator.hasNext()) {
                    foundItem = iterator.next();
                    if (foundItem.getName().equals(name)) {
                        break;
                    }
                }
                restaurant.deleteMenuItem(name);
                //also delete the composite products that contain this base product
                for(int i = 0; i < restaurant.getMenu().size(); i++) {
                    MenuItem current = restaurant.getMenu().get(i);

                    if (current instanceof CompositeProduct && ((CompositeProduct) current).getComponentProducts().contains(foundItem))
                        restaurant.deleteMenuItem(current.getName());
                }
            }
            else if(!name2.equals("") && name.equals("")) //a menuItem of type composite product is going to be deleted
                restaurant.deleteMenuItem(name2);
            else if(name.equals("") && name2.equals("")) //the delete menu item option was selected but the user did not enter a certain product
                adminGUI.warningDelete(); //notice the user to introduce a valid name

            adminGUI.getChosenComponents().setListData(restaurant.getMenu().toArray());
            waiterGUI.getOrderedProducts().setListData(restaurant.getMenu().toArray());
        }
    }

    public class ActionListenerUpdateAdmin implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            //in the case of a base product => available options: change of price
            String name = adminGUI.getName();
            if(!name.equals("")) {
                double oldPrice = 0.0;
                double price = Double.parseDouble(adminGUI.getPrice());
                MenuItem foundItem = null;

                Iterator<MenuItem> iterator = restaurant.getMenu().iterator();
                while (iterator.hasNext()) {
                    foundItem = iterator.next();
                    if (foundItem.getName().equals(name)) {
                        oldPrice = foundItem.computePrice();
                        foundItem.setTotalPrice(price);
                        break;
                    }
                }

                //also change in the composite products that it appears in
                for(int i = 0; i < restaurant.getMenu().size(); i++) {
                    MenuItem current = restaurant.getMenu().get(i);
                    MenuItem newItem = new MenuItem(current.getName(), current.getTotalPrice() - oldPrice + price) {
                        @Override
                        public double computePrice() {
                            return price;
                        }
                    };

                    if (current instanceof CompositeProduct && ((CompositeProduct) current).getComponentProducts().contains(foundItem)) {
                        restaurant.editMenuItem(newItem);
                    }
                }

                adminGUI.getChosenComponents().setListData(restaurant.getMenu().toArray());
            }
            else
                adminGUI.warningEdit(); //notice the user that no name was entered before attempting to edit a certain menu item
        }
    }

    public class ActionListenerListMenuItems implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            MenuTable tableMenu = new MenuTable(restaurant);
        }
    }

    public class OKActionListenerAdmin implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            adminGUI.getFrame().setVisible(false);
            chooseGUI.getFrame().setVisible(true);
        }
    }

    public class ActionListenerInsertWaiter implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            if (waiterGUI.getTable().equals(""))
                waiterGUI.warningInsertTable(); //notice the user that no table nr was entered when attempting to insert a new order
            else if(waiterGUI.getOrderedProducts().isSelectionEmpty())
                waiterGUI.warningInsertChosenProducts(); //notice the user that no products were selected so that the order could take place
            else {
                int table = Integer.parseInt(waiterGUI.getTable());
                Order newOrder = new Order(new Date(), table);
                List choices = waiterGUI.getOrderedProducts().getSelectedValuesList();
                restaurant.createNewOrder(newOrder, choices);
                waiterGUI.setOrderIDField();
            }
        }
    }

    public class ActionListenerComputePrice implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            Map<Order, List<MenuItem>> orders = restaurant.getMappingOrders();
            int orderId = Integer.parseInt(waiterGUI.getOrderID());
            Order o = null;
            for (Order key: orders.keySet())
                if (key.getOrderID() == orderId)
                    o = key;

            double price = restaurant.computePriceOrder(o);
            waiterGUI.setTotalPrice(Double.toString(price));
        }
    }

    public class ActionListenerGenerateBill implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            int orderId = Integer.parseInt(waiterGUI.getOrderID());
            Map<Order, List<MenuItem>> orders = restaurant.getMappingOrders();
            Order o = null;
            for (Order key: orders.keySet())
                if (key.getOrderID() == orderId)
                    o = key;

            restaurant.generateBill(o);
        }
    }

    public class ActionListenerOKWaiter implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            chooseGUI.getFrame().setVisible(true);
            waiterGUI.getFrame().setVisible(false);
        }
    }

    public class ActionListenerViewAllOrders implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            OrderTable ot = new OrderTable(restaurant);
        }
    }

    public class ActionListenerOKChef implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            chefGUI.getFrame().setVisible(false);
            waiterGUI.getFrame().setVisible(true);
        }
    }
}
