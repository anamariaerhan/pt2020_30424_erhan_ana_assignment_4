package businessLayer;

import dataLayer.FileWriter;
import dataLayer.RestaurantSerializator;
import presentationLayer.ChefGraphicalUserInterface;

import java.io.Serializable;
import java.util.*;
import java.util.List;

public class Restaurant extends Observable implements IRestaurantProcessing, Serializable {

    static Map<Order, List<MenuItem>> mappingOrders = new HashMap<Order, List<MenuItem>>(); // keep a hashmap; store the orders
    ArrayList<MenuItem> menu;
    RestaurantSerializator ser = new RestaurantSerializator();
    ChefGraphicalUserInterface chef;

    public Restaurant(ArrayList<MenuItem> menu, ChefGraphicalUserInterface chef) {
        this.chef = chef;
        this.menu = menu;
    }

    public static Map<Order, List<MenuItem>> getMappingOrders(){  // not in interface
        return mappingOrders;
    }

    public void setMappingOrders(HashMap<Order, List<MenuItem>> givenMap) { // not in interface
        this.mappingOrders = givenMap;
    }

    public ArrayList<MenuItem> getMenu() {
        return menu;
    }

    public void setMenu(ArrayList<MenuItem> menu) {
        this.menu = menu;
    }

    /**
     * Creates a new menu item.
     * Precondition: currentMenuItem != null
     * Postcondition: menu.size() = menu.size()@pre+1
     * @param currentMenuItem An object of type Menu Item to be newly created.
     */
    @Override
    public void createNewMenuItem(MenuItem currentMenuItem) {
        assert currentMenuItem != null;

        int currentSize = menu.size();
        menu.add(currentMenuItem);

        assert menu.size() == currentSize + 1;

        ser.RestaurantSerializationOUT(menu);
    }

    /**
     * Deletes a menu item.
     * Precondition: givenName != null
     * Postcondition: menu.size() = menu.size()@pre-1
     * @param givenName A String representing the name of the menu item to be deleted.
     */
    @Override
    public void deleteMenuItem(String givenName) {
        assert givenName != null;
        int currentSize = menu.size();
        assert currentSize > 0;

        menu.removeIf(currentMenuItem -> currentMenuItem.getName().equals(givenName));

        assert menu.size() == currentSize-1;
        ser.RestaurantSerializationOUT(menu);
    }

    /**
     * Edits a menu item.
     * Precondition: currentMenuItem != null
     * Postcondition: menu.size() =  menu.size()@pre
     * @param currentMenuItem An object of type MenuItem to be edited.
     */
    @Override
    public void editMenuItem(MenuItem currentMenuItem) {
        assert currentMenuItem != null;
        int currentSize = menu.size();

        Iterator<MenuItem> it = menu.iterator();
        while (it.hasNext()) {
            MenuItem elemCurrent = (MenuItem)it.next();
            if (elemCurrent.getName().equals(currentMenuItem.getName())) {
                elemCurrent.setTotalPrice(currentMenuItem.totalPrice);
            }
        }

        assert menu.size() == currentSize;
        ser.RestaurantSerializationOUT(menu);
    }

    /**
     * Creates a new order.
     * Precondition: givenOrder != null
     * Precondition: chosenProducts != null
     * Postcondition: mappingOrders.size() = mappingOrders.size()@pre +1
     * @param givenOrder An Order representing the object which will be marked to be created.
     * @param chosenProducts The corresponding chosen products associated with the newly created order.
     */
    @Override
    public void createNewOrder(Order givenOrder, List<MenuItem> chosenProducts) {
        //notify the chef only in the case of a composite product
        assert givenOrder != null;
        assert chosenProducts != null;
        int currentSize = mappingOrders.size();
        boolean notify = false;

        for (MenuItem currentItem: chosenProducts)
            if (currentItem instanceof CompositeProduct)
                notify = true;

        if (notify == true)
            chef.update(this, chosenProducts);

        mappingOrders.put(givenOrder, chosenProducts);

        assert mappingOrders.size() == currentSize + 1;
    }

    /**
     * Computes the total cost of an order.
     * Precondition: givenOrder!=null
     * @param givenOrder An Order representing the object which will be marked to have its total cost computed.
     * @return An object of type double
     */
    @Override
    public double computePriceOrder(Order givenOrder) {
        assert givenOrder != null;

        double finalComputedPrice = 0.0;
        List<MenuItem> currentOrderProducts = mappingOrders.get(givenOrder);

        for (MenuItem currentItem: currentOrderProducts)
            finalComputedPrice += currentItem.computePrice();

        givenOrder.setTotalCost(finalComputedPrice);

        assert finalComputedPrice > 0;
        return finalComputedPrice;
    }

    /**
     * Generates the bill of a certain order.
     * Precondition: givenOrder != null
     * @param givenOrder An Order representing the object which will be marked to have its bill generated.
     */
    @Override
    public void generateBill(Order givenOrder) {
        assert givenOrder != null;
        FileWriter.writeBillToFile(givenOrder);
    }

    public boolean isWellFormed() {
        Set<Order> set = mappingOrders.keySet();

        for(Order o: set) {
            if (mappingOrders.get(o) == null)
                return false;
        }
        return true;
    }
}
