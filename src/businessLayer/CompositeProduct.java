package businessLayer;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class CompositeProduct extends MenuItem {
    List<MenuItem> componentProducts;

    public CompositeProduct(String name, double totalPrice) {
        super(name, totalPrice);
        componentProducts = new ArrayList<MenuItem>();
    }

    public List<MenuItem> getComponentProducts() {
        return componentProducts;
    }

    @Override
    public double computePrice() {
        double finalComputedPrice = 0.0;

        for (MenuItem currentMenuItem: componentProducts)
            finalComputedPrice += currentMenuItem.computePrice();

        super.totalPrice = finalComputedPrice;

        return finalComputedPrice;
    }

    public void addItem(MenuItem givenItem) {
        componentProducts.add(givenItem);
    }

    public void removeItem(MenuItem givenItem) {
        componentProducts.remove(givenItem);
    }
}

