package businessLayer;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

public class Order {
    int orderID;
    Date date; //creation date
    int table;
    double totalCost;
    //ArrayList<MenuItem> orderedProducts;
    private static int index = 0;

    DateFormat df = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");

    public int getTable() {
        return table;
    }

    public void setTable(int table) {
        this.table = table;
    }

    public int getOrderID() {
        return orderID;
    }

    public void setOrderID(int orderID) {
        this.orderID = orderID;
    }

    public double getTotalCost() {
        return totalCost;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public void setTotalCost(double totalCost) {
        this.totalCost = totalCost;
    }

    public Order(Date date, int table) {
        this.orderID = index++;
        this.date = date;
        this.table = table;
    }

    //@Override
    public int hashcode() {
        int index = 7;
        int primeNr = 31;
        index = primeNr * index + orderID;
        index = primeNr * index + (int)date.hashCode();
        index = primeNr * index + table;
        return index;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Order other = (Order)obj;
        boolean dateStatus;
        if(date.compareTo(other.date) == 0)
            dateStatus = false; //dates are equal
        else
            dateStatus = true; //dates are not equal
        return ((orderID != other.orderID)
                && (table != other.table))
                && dateStatus;
                //&& listsStatus;
    }
}
