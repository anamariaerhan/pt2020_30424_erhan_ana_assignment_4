package businessLayer;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;

public interface IRestaurantProcessing {

    public void createNewMenuItem(MenuItem givenMenuItem);

    public void deleteMenuItem(String givenName);

    public void editMenuItem(MenuItem givenMenuItem);

    public void createNewOrder(Order givenOrder, List<MenuItem> menuItemsList);

    public double computePriceOrder(Order givenOrder);

    public void generateBill(Order givenOrder);
}
