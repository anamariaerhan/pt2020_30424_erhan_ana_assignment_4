package businessLayer;

import java.util.Date;

public class BaseProduct extends MenuItem {
    public BaseProduct(String name, double totalPrice) {
        super(name, totalPrice);
    }

    @Override
    public double computePrice() {
        return this.totalPrice;
    }
}
