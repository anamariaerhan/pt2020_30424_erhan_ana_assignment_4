package businessLayer;

public abstract class MenuItem implements Comparable<Object> {
    String name; //for differentiation
    double totalPrice;

    public MenuItem() { }

    public MenuItem(String name, double totalPrice) {
        this.name = name;
        this.totalPrice = totalPrice;
    }

    public double getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(double givenTotalPrice) {
        this.totalPrice = givenTotalPrice;
    }

    public String getName() {
        return name;
    }

    public abstract double computePrice();

    @Override
    public String toString() {
        return "Name: " + name + " totalPrice: " + totalPrice;
    }

    //method used for the equals method in Order class(had to be implemented for comparing 2 ArrayLists)
    @Override
    public int compareTo(Object o) {
        MenuItem elem = (MenuItem) o;
        return this.name.compareTo(elem.name);
    }
}
