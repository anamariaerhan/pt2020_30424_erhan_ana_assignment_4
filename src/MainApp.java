import businessLayer.*;
import controller.MainController;
import dataLayer.RestaurantSerializator;
import presentationLayer.AdministratorGraphicalUserInterface;
import presentationLayer.ChefGraphicalUserInterface;
import presentationLayer.ChoiceGUI;
import presentationLayer.WaiterGraphicalUserInterface;

import java.util.ArrayList;
import java.util.Date;

public class MainApp {
    public static void main(String[] args) {
        MenuItem item1 = new BaseProduct("Potatoes",10.0);
        MenuItem item2 = new BaseProduct("Steak", 25.0);
        MenuItem item3 = new CompositeProduct("Potatoes and steak",45.0);
        ((CompositeProduct)item3).addItem(item1);
        ((CompositeProduct)item3).addItem(item2);

        ArrayList<MenuItem> menu = new ArrayList<MenuItem>();
        menu.add(item1);
        menu.add(item2);
        menu.add(item3);

        RestaurantSerializator ser=new RestaurantSerializator();
        menu = ser.RestaurantDeserializationIN();

        ChefGraphicalUserInterface chef = new ChefGraphicalUserInterface();
        Restaurant restaurant = new Restaurant(menu, chef);
        Order order = new Order(new Date(),1);
        ArrayList<MenuItem> orderList = new ArrayList<MenuItem>();
        orderList.add(item1);
        orderList.add(item3);
        AdministratorGraphicalUserInterface admin = new AdministratorGraphicalUserInterface(menu);
        WaiterGraphicalUserInterface waiter = new WaiterGraphicalUserInterface(restaurant);
        ChoiceGUI main = new ChoiceGUI();
        MainController c = new MainController(main, admin, waiter, chef, restaurant);
    }
}
