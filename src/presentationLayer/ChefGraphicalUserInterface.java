package presentationLayer;

import java.awt.event.ActionListener;
import java.util.List;
import java.util.Observable;
import java.util.Observer;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import businessLayer.MenuItem;

public class ChefGraphicalUserInterface extends JFrame implements Observer {
        JTextArea ta = new JTextArea(20, 5);
        JTextField sb = new JTextField(22);
        JButton btnOK = new JButton("OK(return)");

        public JFrame getFrame() {
            return this;
        }

        public ChefGraphicalUserInterface() {
            JPanel first = new JPanel();
            JPanel second = new JPanel();
            JPanel total = new JPanel();
            total.add(first);
            sb.setEditable(false);
            total.add(second);
            first.setLayout(new BoxLayout(first, BoxLayout.Y_AXIS));
            JScrollPane sp = new JScrollPane(ta);
            ta.setEditable(false);
            first.add(new JLabel("Current order:"));
            first.add(sp);
            first.add(btnOK);
            this.setContentPane(total);
            this.setSize(700, 550);
            this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
            this.setTitle("Chef");
        }

        @Override
        public void update(Observable arg0, Object arg1) {
            List<MenuItem> order = (List<MenuItem>) arg1;
            this.setVisible(true);
            ta.setText("");

            for (MenuItem item : order) {
                ta.append(item.getName());
                ta.append("\n");
            }
        }

        public void addOKActionListener(ActionListener a) { // OK - return to Waiter GUI
            btnOK.addActionListener(a);
        }
}
