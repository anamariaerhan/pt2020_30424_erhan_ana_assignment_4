package presentationLayer;

import javax.swing.JFrame;
import javax.swing.JLabel;

import java.awt.Font;
import java.awt.event.ActionListener;

import javax.swing.JButton;

public class ChoiceGUI {
    private JFrame frame;
    public  JButton btnWaiter;
    public  JButton btnAdministrator;

    public ChoiceGUI() {
        initialize();
    }

    private void initialize() {
        frame = new JFrame();
        frame.setBounds(100, 100, 530, 485);
        frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        frame.getContentPane().setLayout(null);

        JLabel topLabel = new JLabel("Restaurant Management");
        topLabel.setFont(new Font("Serif", Font.BOLD, 26));
        topLabel.setBounds(130, 100, 300, 40);
        frame.getContentPane().add(topLabel);

        btnAdministrator = new JButton("Administrator");
        btnAdministrator.setBounds(88, 257, 150, 25);
        frame.getContentPane().add(btnAdministrator);

        btnWaiter = new JButton("Waiter");
        btnWaiter.setBounds(339, 257, 97, 25);
        frame.getContentPane().add(btnWaiter);
    }

    public JFrame getFrame() {
        return frame;
    }

    public void addActionListenerWaiter(ActionListener a) {
        btnWaiter.addActionListener(a);
    }

    public void addActionListenerAdministrator(ActionListener a) {
        btnAdministrator.addActionListener(a);
    }
}
