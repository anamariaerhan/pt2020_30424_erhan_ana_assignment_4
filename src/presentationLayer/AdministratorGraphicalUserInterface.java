package presentationLayer;

import businessLayer.MenuItem;

import javax.swing.*;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class AdministratorGraphicalUserInterface extends JFrame {

    JButton btnInsert = new JButton("CREATE NEW MENU ITEM");
    JButton btnDelete = new JButton("DELETE MENU ITEM");
    JButton btnUpdate = new JButton("EDIT MENU ITEM");

    JTextField name = new JTextField(20);
    JTextField price = new JTextField("0");
    JTextField name2 = new JTextField(20);

    JButton btnOK = new JButton("OK(return to main application)");
    JButton btnListMenuItems =new JButton("LIST MENU ITEMS");
    JList chosenComponents;
    ArrayList<MenuItem> menuItems;
    DefaultListModel<String> listModel;

    public AdministratorGraphicalUserInterface(ArrayList<MenuItem> menuItems) {
        this.menuItems = menuItems;
        JPanel p = new JPanel();
        p.setLayout(new BoxLayout(p, BoxLayout.Y_AXIS));

        p.add(new JLabel("Base product:"));
        p.add(new JLabel("Name:"));
        p.add(name);
        p.add(new JLabel("Price:"));
        p.add(price);
        p.add(new JLabel("Composite product:"));
        p.add(new JLabel("Name:"));
        p.add(name2);

        JPanel butoane = new JPanel();
        butoane.setLayout(new BoxLayout(butoane, BoxLayout.Y_AXIS));
        butoane.add(btnInsert);
        butoane.add(btnDelete);
        butoane.add(btnUpdate);
        butoane.add(btnListMenuItems);

        JPanel total = new JPanel();
        total.add(p);
        total.add(butoane);

        chosenComponents = new  JList<>(menuItems.toArray());
        chosenComponents.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
        JScrollPane sp = new JScrollPane(chosenComponents);
        p.add(new JLabel("Choose the components of the newly created composite product:"));
        p.add(sp);

        JPanel jos = new JPanel();
        jos.add(btnOK);

        JPanel totalFinal = new JPanel();
        totalFinal.setLayout(new BoxLayout(totalFinal, BoxLayout.Y_AXIS));
        totalFinal.add(total);
        totalFinal.add(jos);
        this.setContentPane(totalFinal);
        this.setSize(700, 450);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setTitle("Administrator");
    }

    public JList getChosenComponents() {
        return chosenComponents;
    }

    public void setNameTextField(String givenString) {
        name.setText(givenString);
    }

    public void setName2TextField(String givenString) {
        name2.setText(givenString);
    }

    public void setChosenComponents(JList chosenComponents) {
        this.chosenComponents = chosenComponents;
    }

    public JFrame getFrame() {
        return this;
    }

    public String getName() {
        return name.getText();
    }

    public void warningDelete() {
        JOptionPane.showMessageDialog(getFrame(), "Please, introduce a valid name for the product to be deleted!", "Warning", JOptionPane.WARNING_MESSAGE);
    }

    public void warningInsert() {
        JOptionPane.showMessageDialog(getFrame(), "Please, introduce a valid name for the product to be created!", "Warning", JOptionPane.WARNING_MESSAGE);
    }

    public void warningEdit() {
        JOptionPane.showMessageDialog(getFrame(), "Please, introduce a valid name for the product to be edited!", "Warning", JOptionPane.WARNING_MESSAGE);
    }

    public String getName2() {
        return name2.getText();
    }

    public String getPrice() {
        String s = price.getText();
        Pattern p = Pattern.compile("-?\\d+(\\.\\d+)?");//check if price is of numeric type(int/float)
        Matcher m = p.matcher(s);

        if (m.find() && m.group().equals(s))
            return s;
        else {
            JOptionPane.showMessageDialog(getFrame(), "Please, introduce a valid price!", "Warning", JOptionPane.WARNING_MESSAGE);
            return null;
        }
    }

    public void setPrice(String s) {
        price.setText(s);
    }

    public void addInsertActionListener(ActionListener a) { // insert
        btnInsert.addActionListener(a);
    }

    public void addDeleteActionListener(ActionListener a) { // delete
        btnDelete.addActionListener(a);
    }

    public void addUpdateActionListener(ActionListener a) { // update
        btnUpdate.addActionListener(a);
    }

    public void addListMenuItemsActionListener(ActionListener a) { // list MenuItems
        btnListMenuItems.addActionListener(a);
    }

    public void addOKActionListener(ActionListener a) { // OK - return to Choice GUI
        btnOK.addActionListener(a);
    }
}