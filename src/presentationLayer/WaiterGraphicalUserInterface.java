package presentationLayer;

import businessLayer.MenuItem;
import businessLayer.Order;
import businessLayer.Restaurant;

import javax.swing.*;

import java.awt.event.ActionListener;

public class WaiterGraphicalUserInterface extends JFrame {

    Restaurant restaurant;
    JButton btnInsert = new JButton("CREATE NEW ORDER");
    JButton btnComputePrice = new JButton("COMPUTE TOTAL PRICE");
    JButton btnGenerateBill = new JButton ("GENERATE BILL");
    JButton btnOK = new JButton("OK(return to main application)");
    JButton btnViewAllOrders = new JButton("VIEW ALL ORDERS");

    JTextField table = new JTextField(5);
    JTextField orderID = new JTextField(5);
    JTextField totalPrice = new JTextField(5);
    JList orderedProducts;

    static int i = 1;

    public void setTable(String s) {
        this.table.setText(s);
    }

    public void setOrderID(String s) {
        this.orderID.setText(s);
    }

    public String getTable() {
        return this.table.getText();
    }

    public String getOrderID() {
        return orderID.getText();
    }

    public WaiterGraphicalUserInterface(Restaurant givenRestaurant) {
        this.restaurant = givenRestaurant;

        JPanel principal = new JPanel();
        orderedProducts = new JList<>(restaurant.getMenu().toArray());
        orderedProducts.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
        JScrollPane sp = new JScrollPane(orderedProducts);
        principal.add(new JLabel("Order ID(computed automatically - do not complete field!)"));
        principal.add(orderID);
        principal.add(new JLabel("Table"));
        principal.add(table);

        principal.add(new JLabel("Choose the ordered products:"));
        principal.add(sp);
        principal.add(new JLabel("Total payment:"));
        principal.add(totalPrice);
        principal.setLayout(new BoxLayout(principal, BoxLayout.Y_AXIS));

        JPanel butoane = new JPanel();
        butoane.setLayout(new BoxLayout(butoane, BoxLayout.Y_AXIS));
        butoane.add(btnInsert);
        butoane.add(btnComputePrice);
        butoane.add(btnGenerateBill);
        butoane.add(btnViewAllOrders);

        JPanel down = new JPanel();
        down.add(btnOK);

        JPanel up = new JPanel();
        up.add(principal);
        up.add(butoane);

        JPanel total = new JPanel();
        total.setLayout(new BoxLayout(total, BoxLayout.Y_AXIS));
        total.add(up);
        total.add(down);

        this.setContentPane(total);
        this.setSize(700, 450);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setTitle("Waiter");
    }

    public JList getOrderedProducts() {
        return orderedProducts;
    }

    public void setOrderedProducts(JList orderedProducts) {
        this.orderedProducts = orderedProducts;
    }

    public void setTotalPrice(String total) {
        this.totalPrice.setText(total);
    }

    public JFrame getFrame() {
        return this;
    }

    public void warningInsertTable() {
        JOptionPane.showMessageDialog(getFrame(), "Please, introduce a valid table number for the order to be created!", "Warning", JOptionPane.WARNING_MESSAGE);
    }

    public void warningInsertChosenProducts() {
        JOptionPane.showMessageDialog(getFrame(), "Please, select the desired products for the order to be created!", "Warning", JOptionPane.WARNING_MESSAGE);
    }

    public void setOrderIDField() {
        orderID.setText(String.valueOf(i++));
    }

    public void addInsertActionListener(ActionListener a) { // insert order
        btnInsert.addActionListener(a);
    }

    public void computePriceActionListener(ActionListener a) { // compute price
        btnComputePrice.addActionListener(a);
    }

    public void generateBillActionListener(ActionListener a) { // generate bill
        btnGenerateBill.addActionListener(a);
    }

    public void viewAllOrdersActionListener(ActionListener a) { // OK - return to Choice GUI
        btnViewAllOrders.addActionListener(a);
    }

    public void addOKActionListener(ActionListener a) { // OK - return to Choice GUI
        btnOK.addActionListener(a);
    }
}